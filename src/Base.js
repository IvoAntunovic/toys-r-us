import Rebase from 're-base';
import firebase from "firebase";
import 'firebase/database';
 
const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyCCKsA0nALUnP4gsseI_1tD632HbWlyIXs",
    authDomain: "toys-r-us-a00b1.firebaseapp.com",
    projectId: "toys-r-us-a00b1",
    storageBucket: "toys-r-us-a00b1.appspot.com",
    messagingSenderId: "861202694215",
    appId: "1:861202694215:web:cbb859d93624e1c5f67162"
});
 
const base = Rebase.createClass(firebase.database())
 
export { firebaseApp }
export default base
