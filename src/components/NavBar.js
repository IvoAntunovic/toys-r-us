import react, { Component } from 'react';

export default class NavBar extends Component
{
    render()
    {
        return(
                 <div className="nav-bar-container">
                      <div className="nav-bar">
                            <div className="link-container">
                                <div>
                                   <a href="#">Tous les jouets</a>
                                </div>
                                <div>
                                    <a href="#">Par marque</a>
                                </div>
                            </div> 
                      </div>
                 </div>
        );
    }
}