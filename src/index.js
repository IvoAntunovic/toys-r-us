import React from "react";
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Home from "./components/Home";
import Detail from "./components/Detail";
import List from "./components/List";
import NotFound from "./components/NotFound";
import StyleCss from "./style.css";

const Root = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/detail/:detail' component={Detail} />
            <Route path='/list/' component={List} />
            <Route path='/list/:brand' component={List} />
            <Route component={NotFound}/>
        </Switch>
    </BrowserRouter>
)

ReactDOM.render(<Root/>, document.querySelector('#root'));

